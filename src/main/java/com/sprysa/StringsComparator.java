package com.sprysa;

import java.util.Arrays;

public class StringsComparator {

  private String[] arr = {"Ukraine Kyiv", "Poland Warsaw", "Germany Berlin", "France Paris",
      "United_Kingdom London", "United_States Washington"};

  public String[] sortByFirstWord() {
    Arrays.sort(arr);
    return arr;
  }

  public String[] sortBySecondWord() {
    String[] temparr = new String[arr.length];
    String[] retunarr = new String[arr.length];
    for (int i = 0; i < arr.length; i++) {
      temparr[i] = arr[i].substring(arr[i].indexOf(" ") + 1, arr[i].length());
    }
    Arrays.sort(temparr);
    for (int i = 0; i < temparr.length; i++) {
      for (int j = 0; j < arr.length; j++) {
        if (arr[j].contains(temparr[i])) {
          retunarr[i] = arr[j];
          break;
        }
      }
    }
    return retunarr;
  }

  public static void main(String[] args) {
    String[] arr1 = new StringsComparator().sortByFirstWord();
    System.out.println("Sorted by first word:");
    for (int i = 0; i < arr1.length; i++) {
      System.out.println(arr1[i]);
    }
    String[] arr2 = new StringsComparator().sortBySecondWord();
    System.out.println("Sorted by second word:");
    for (int i = 0; i < arr2.length; i++) {
      System.out.println(arr2[i]);
    }
  }
}
