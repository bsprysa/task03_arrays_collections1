package com.sprysa;

import java.util.ArrayList;

public class StringContainer {

  private String[] arr;

  public void add(String s) {
    if (arr == null) {
      arr = new String[1];
      arr[0] = s;
    } else {
      String[] temparr = arr;
      arr = new String[arr.length + 1];
      System.arraycopy(temparr, 0, arr, 0, temparr.length);
      arr[arr.length - 1] = s;
    }
  }

  public String get(int i) {
    return arr[i];
  }

  public static void main(String[] args) {
    System.out.println("Comparing performance between StringContainer and ArrayList<String>:");
    long time1, time2, time3;
    StringContainer sc = new StringContainer();
    ArrayList<String> al = new ArrayList<String>();
    time1 = System.nanoTime();
    sc.add("First element");
    time2 = System.nanoTime();
    al.add("First element");
    time3 = System.nanoTime();
    System.out.println(
        "Time of adding first element in StringContainer (in nanoseconds): " + (time2 - time1));
    System.out.println(
        "Time of adding first element in ArrayList<String> (in nanoseconds): " + (time3 - time2));
    time1 = System.nanoTime();
    sc.add("Second element");
    time2 = System.nanoTime();
    al.add("Second element");
    time3 = System.nanoTime();
    System.out.println(
        "Time of adding second element in StringContainer (in nanoseconds): " + (time2 - time1));
    System.out.println(
        "Time of adding second element in ArrayList<String> (in nanoseconds): " + (time3 - time2));
    time1 = System.nanoTime();
    sc.get(1);
    time2 = System.nanoTime();
    al.get(1);
    time3 = System.nanoTime();
    System.out.println(
        "Time of geting second element from StringContainer (in nanoseconds): " + (time2 - time1));
    System.out.println(
        "Time of geting second element from ArrayList<String> (in nanoseconds): " + (time3
            - time2));
  }
}
